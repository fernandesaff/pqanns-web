import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import axios from 'axios';
import { makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Redirect } from 'react-router-dom'

const useStyles = makeStyles(theme => ({
    input: {
        display: 'none',
    },
    item: {
        width: '100%',
    },
    voidSpace: {
        minHeight: '22vh'
    },
    voidSpace2: {
        minHeight: '5vh'
    },
    voidSpace3: {
        minHeight: '24vh'
    },
    centerText: {
        textAlign: 'center',
    },
    button: {
        width: '240px'
    }
}));


export default function SearchBox(props) {
    let [selectedFile, setSelectedFile] = useState(null)
    let [img, setImg] = useState(null)
    let [urls, setUrls] = useState([])
    let [fileText, setFileText] = useState('Selecione a Imagem')
    let [redirect, setRedirect] = useState(false);

    const classes = useStyles();

    function onClickHandler() {
        const data = new FormData()
        data.append('file', selectedFile)
        axios.post("http://localhost:8000/upload", data, {}).then(res => { // then print response status
            setImg('data:' + res.data[0].mimetype + ';base64,' + arrayBufferToBase64(res.data[0].buffer.data));
            setUrls(res.data[1]);
            setRedirect(true);
        })
    }

    function onChangeHandler(event) {
        setSelectedFile(event.target.files[0]);
        setFileText(event.target.files[0].name);
    }
    
    function arrayBufferToBase64(buffer) {
        var binary = '';
        var bytes = [].slice.call(new Uint8Array(buffer));
        bytes.forEach((b) => binary += String.fromCharCode(b));
        return window.btoa(binary);
    }

    return (
        <>
            {redirect ? <Redirect to={{pathname: '/search', state: { urls: urls, img: img, fileText: fileText}}} /> : null}
            <Box bgcolor="secondary.main" color="white.main">
                <Grid>
                    <div className={classes.voidSpace}></div>
                    <Grid item>
                        <Grid container justify="center" spacing={2} className={classes.item}>
                            <Grid item>
                            <Typography variant="h4" color="inherit" className={classes.centerText}>
                                Busca Reversa de Imagens
                            </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <div className={classes.voidSpace2}></div>
                    <Grid item>
                        <Grid container justify="center" spacing={2} className={classes.item}>
                            <Grid item>
                                <input accept="image/*" className={classes.input} id="contained-button-file" type="file" onChange={onChangeHandler}/>
                                <label htmlFor="contained-button-file" >
                                    <Button variant="contained" color="primary" component="span" className={classes.button}>
                                        <Typography noWrap>
                                            {fileText}
                                        </Typography>
                                    </Button>
                                </label>
                            </Grid>
                            <Grid item>
                                <Button variant="contained" color="primary" onClick={onClickHandler}>
                                    <Typography noWrap>
                                        Buscar
                                    </Typography>
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                    <div className={classes.voidSpace3}></div>
                </Grid>
            </Box>
        </>
    );
};