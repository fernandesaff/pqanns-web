import React from 'react';
import OpenSans from './OpenSans-Regular.ttf'
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import SearchBox from './SearchBox';
import Topbar from './Topbar'
import ExplanationText from './ExplanationText'
import { makeStyles} from '@material-ui/core/styles';

const openSans = {
  fontFamily: 'OpenSans',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 400,
  src: `
    local('OpenSans'),
    local('OpenSans-Regular'),
    url(${OpenSans}) format('ttf')
  `,
  unicodeRange: 'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};

const theme = createMuiTheme({
  palette: {
    primary: { main: '#00084D' }, 
    secondary: { main: '#1F2666' }, 
    white:{ main: '#FFFFFF' },
    black:{ main: '#000000' },
  },
  typography: {
    fontFamily: 'OpenSans',
    fontSize: 14,
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [openSans],
      },
    },
  },
});

const useStyles = makeStyles(theme => ({
  // Load app bar information from the theme
  toolbar: theme.mixins.toolbar
}));

export default function App(props) {
  const classes = useStyles();

  return (  
    <ThemeProvider theme={theme}>
      {Topbar()}
      <div className={classes.toolbar} />
      {SearchBox()}
      {ExplanationText()}

    </ThemeProvider>
  );
}