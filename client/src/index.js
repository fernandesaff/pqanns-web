import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './index.css';
import App from './App.js';
import Search from './Search.js';

const routing = (
    <Router>
        <div>
        <Route exact path="/" component={App} />
        <Route path="/search" component={Search} />
        </div>
    </Router>
)


ReactDOM.render(routing , document.getElementById("root") );