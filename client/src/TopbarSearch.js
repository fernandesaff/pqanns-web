import React, { useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import List from '@material-ui/core/List';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Link from "@material-ui/core/Link";
import { fade, makeStyles} from '@material-ui/core/styles';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Redirect } from 'react-router-dom'
import { Divider } from '@material-ui/core';

export default function TopbarSearch(props) {

    const useStyles = makeStyles(theme => ({
        menuButton: {
            marginRight: theme.spacing(2),
        },
        text: {
            color: "#00084D",
        },
        input: {
            display: 'none',
        },
        buttonImg: {
            margin: theme.spacing(1),
            width: '240px',

        },
        button: {
            margin: theme.spacing(1),
        },
        search: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginRight: theme.spacing(2),
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(3),
                width: 'auto',
            },
        },
    }));

    let [selectedFile, setSelectedFile] = useState(null)
    let [fileText, setFileText] = useState('Selecione a Imagem')
    let [img, setImg] = useState(null)
    let [urls, setUrls] = useState([])
    let [redirect, setRedirect] = useState(false);
    let [fileName, setFileName] = useState(null);

    const classes = useStyles();

    function onClickHandler() {
        const data = new FormData()
        data.append('file', selectedFile)
        axios.post("http://localhost:8000/upload", data, {}).then(res => { // then print response status
            setImg('data:' + res.data[0].mimetype + ';base64,' + arrayBufferToBase64(res.data[0].buffer.data));
            setUrls(res.data[1]);
            setFileName(fileText);
            setFileText('Selecione a Imagem');
            setRedirect(true);
        })
    }

    function onChangeHandler(event) {
        setSelectedFile(event.target.files[0]);
        setFileText(event.target.files[0].name);
    }
    
    function arrayBufferToBase64(buffer) {
        var binary = '';
        var bytes = [].slice.call(new Uint8Array(buffer));
        bytes.forEach((b) => binary += String.fromCharCode(b));
        return window.btoa(binary);
    }

    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });
    
    const toggleDrawer = (side, open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
    
        setState({ ...state, [side]: open });
    };
    
    const sideList = side => (
        <div
          className={classes.list}
          role="presentation"
          onClick={toggleDrawer(side, false)}
          onKeyDown={toggleDrawer(side, false)}
        >
            <List>
                {['Início', 'Como Funciona', 'Sobre o Projeto', 'Contato'].map((text, index) => (
                <ListItem button key={text}>
                    <ListItemText  primary={text} className={classes.text}/>
                </ListItem>
                ))}
            </List>
        </div>
    );

    return (
        <>
            {redirect ? <Redirect to={{pathname: '/search', state: { urls: urls, img: img, fileText: fileName}}} /> : null}
            <AppBar position="fixed" elevation={0} color="inherit">
                <Toolbar>

                    <IconButton edge="start" className={classes.menuButton} color="primary" aria-label="menu" onClick={toggleDrawer('left', true)}>
                        <MenuIcon/>
                    </IconButton>

                    <Link href="/" variant="h5" color="primary" style={{ textDecoration: 'none' }}>
                        PQANNS
                    </Link>

                    <div className={classes.search}>
                        <input accept="image/*" className={classes.input} id="contained-button-file" type="file" onChange={onChangeHandler}/>
                        <label htmlFor="contained-button-file" >
                            <Button variant="outlined" color="primary" component="span" className={classes.buttonImg}>
                                <Typography noWrap>
                                    {fileText}
                                </Typography>
                            </Button>
                        </label>

                        <Button variant="outlined" color="primary" onClick={onClickHandler} className={classes.button}>
                            <Typography noWrap>
                                Buscar
                            </Typography>
                        </Button>
                    </div>
                </Toolbar>
                <Divider component="li"/>
            </AppBar>

            <SwipeableDrawer
                open={state.left}
                onClose={toggleDrawer('left', false)}
                onOpen={toggleDrawer('left', true)}
            >
                {sideList('left')}
            </SwipeableDrawer> 
        </>
    );
};