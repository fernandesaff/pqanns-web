import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles} from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

function Result(props) {
    const urls = props.urls;
    const img = props.img;
    const fileText = props.fileText;

    const useStyles = makeStyles(theme => ({
        item: {
            width: '100%',
            marginLeft: 2,
            marginRight: 2,
            marginTop: 2,
            marginBottom: 2,
        }
    }));

    const classes = useStyles();

    return(
    <Grid container spacing={2} className={classes.item}>
        <Grid item xs={12}></Grid>
        <Grid item xs={12}>
            <Grid container justify="right" spacing={2} className={classes.item}>
                <Grid key="query" item>
                    <img src={img} alt='' style={{height: '30vh'}}/>
                </Grid>
                <Grid key="metadata" item>
                    <Typography color="primary">{fileText}</Typography>
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <Grid container justify="center" spacing={2} className={classes.item}>
                <Typography variant="h6" color="primary">Resultados</Typography>
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <Grid container justify="center" spacing={2} className={classes.item}>
                {urls.map((value, index) => (
                    <Grid key={value} item>
                        <img src={value} alt='' key={index} style={{height: '30vh'}}/>
                    </Grid>
                ))}
            </Grid>
        </Grid>
        <Grid item xs={12}></Grid>
    </Grid> 
    );
}

export default Result;