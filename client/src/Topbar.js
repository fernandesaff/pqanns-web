import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import List from '@material-ui/core/List';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Box, Link } from "@material-ui/core";
import { makeStyles} from '@material-ui/core/styles';

function Topbar(props) {

    const useStyles = makeStyles(theme => ({
        menuButton: {
            marginRight: theme.spacing(2),
        },
        text: {
            color: "#00084D",
        }
    }));

    const classes = useStyles();

    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });
    
    const toggleDrawer = (side, open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
    
        setState({ ...state, [side]: open });
    };
    
    const sideList = side => (
        <div
          className={classes.list}
          role="presentation"
          onClick={toggleDrawer(side, false)}
          onKeyDown={toggleDrawer(side, false)}
        >
            <List>
                {['Tutorial', 'Como Funciona', 'Sobre o Projeto', 'Contato'].map((text, index) => (
                <ListItem button key={text}>
                    <ListItemText  primary={text} className={classes.text}/>
                </ListItem>
                ))}
            </List>
        </div>
    );

    return (
        <Box>
            <AppBar position="fixed" elevation={0} color="inherit">
                <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="primary" aria-label="menu" onClick={toggleDrawer('left', true)}>
                    <MenuIcon/>
                </IconButton>
                <Link href="/" variant="h5" color="primary" style={{ textDecoration: 'none' }}>
                    PQANNS
                </Link>
                </Toolbar>
            </AppBar>

            <SwipeableDrawer
                open={state.left}
                onClose={toggleDrawer('left', false)}
                onOpen={toggleDrawer('left', true)}
            >
                {sideList('left')}
            </SwipeableDrawer> 
        </Box>
    );
};

export default Topbar;