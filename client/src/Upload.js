import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing(2),
  },
}));

export default function Upload() {
  const [spacing, setSpacing] = React.useState(2);
  const classes = useStyles();

  const handleChange = event => {
    setSpacing(Number(event.target.value));
  };

  return (
    <Grid container className={classes.root} spacing={2}>
        <header className="App-header">
            <h1>Busca Reversa de Imagens</h1>
          <div>
            <input accept="image/*" className="inputfile" id="contained-button-file" type="file" onChange={this.onChangeHandler}/>
            <label htmlFor="contained-button-file">
              <Button variant="contained" component="span" className={classes.button}>
                <MdFileUpload/>{this.state.fileText}
              </Button>
            </label>
            <Button variant="contained" color="primary" onClick={this.onClickHandler}>Buscar</Button>
          </div>
        </header>
        <div>
          <img src={this.state.img} alt='' style={{height: '30vh'}}/>
        </div>

        {this.state.urls.map((value, index) => {
          return <img src={value} alt='' key={index} style={{height: '30vh'}}/>
        })}
    </Grid>
  );
}