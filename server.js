var express = require('express');
var app = express();
var multer = require('multer')
var cors = require('cors');
var net = require('net');

const nthline = require('nthline')
    , filePath = './public/image-urls-train.txt'

var client = new net.Socket();

app.use(cors())

var storage = multer.memoryStorage()

var upload = multer({ storage: storage }).single('file')

app.post('/upload', function (req, res) {
    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }

        client.connect(8001, '127.0.0.1', function () {
            console.log('Connected');
            console.log(req.file.buffer);
            console.log(client.write(req.file.buffer), 'binary');
            client.end();
        });

        client.on('data', function (data) {
            client.destroy();

            (async () => {

                var urls = [];
            
                var i = 0;
                
                while(i < data.length) {
                    urls.push(await nthline(data.readInt32LE(i), filePath)
                    .then(line => {
                        infos = line.split("\t");
                        return infos[1];
                        }).catch(e => console.log(e)))
                    i+= 4;
                }

                console.log(urls)
                var response = [req.file, urls]
        
                return res.status(200).send(response)
            })()
        });
    })
});

app.listen(8000, function () {
    console.log('App running on port 8000');
});

client.on('drain', function () {
    console.log('All Buffer sent');
});

client.on('close', function () {
    console.log('Connection closed');
});

client.on('end', function () {
    console.log('Connection closed');
});